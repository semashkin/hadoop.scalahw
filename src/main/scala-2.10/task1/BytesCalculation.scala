package task1

import org.apache.spark.rdd.RDD

/**
  * Created by Semashkin on 05.09.2016.
  */
case class BytesCalculationValues(ip: String, avg: Double, total: Int)

object BytesCalculation {
  def Run(lines: RDD[String]): RDD[BytesCalculationValues] = {
    val result = lines.map(line=>line.split(" (\".*?\")?"))
      .filter(a=>a(7) forall Character.isDigit)
      .map(a => (a(0), (a(7).toInt,1)))
      .reduceByKey((a,b)=>(a._1 + b._1,a._2 + b._2))
      .map(a=>BytesCalculationValues(a._1,a._2._1.toDouble/a._2._2.toDouble,a._2._1))

    result
  }
}