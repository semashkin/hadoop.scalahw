package task1
    import org.apache.spark.SparkContext
    import org.apache.spark.SparkContext._
    import org.apache.spark.SparkConf
    import task1.{BytesCalculation, UserAgentCalculation}

    object Task1 {
      def main(args: Array[String]) {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Simple Application")
    val sc = new SparkContext(conf)
    val rdd = sc.textFile("hdfs:///demo/Spark/Task1/000000", 2).cache()
    val result = BytesCalculation.Run(rdd)
    result.collect().foreach(println)
    val result2 = UserAgentCalculation.Run(rdd)
    result2.collect().foreach(println)
  }
}