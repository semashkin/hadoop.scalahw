package task1

import org.apache.spark.rdd.RDD

/**
  * Created by Semashkin on 05.09.2016.
  */
case class UserAgentStat(browser: String, cnt: Int)

object UserAgentCalculation{
  def Run(lines: RDD[String]):RDD[UserAgentStat]={
    val regexpr = "(?<=\" \").*".r;
    val result = lines.map(line=>(UserAgentParser.getBrowser(regexpr.findFirstIn(line).get),line.split(" (\".*?\")?")))
      .map(a=>(a._1,a._2(0)))
      .groupByKey()
      .map(a => (a._1,a._2.toArray.distinct.length))
      .map(a=>UserAgentStat(a._1,a._2))

    result
  }
}

object UserAgentParser {
  def getBrowser(ua: String) : String={
    /*val u = ua.replace("\"","");
    println(u);
    val userAgent = UserAgent.parseUserAgentString(u)
    val browser = userAgent.getBrowser().getName()*/
    val browser = "[a-zA-Z0-1]+".r.findFirstIn(ua)

    if (browser.isDefined){
      browser.get
    }
    else{
      "undefined"
    }
  }
}
