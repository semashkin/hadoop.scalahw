package task2

import org.apache.spark.sql.DataFrame

/**
  * Created by Semashkin on 11.09.2016.
  */
object AirportService {
  def getFlightPerAiport(flights: DataFrame, airports: DataFrame)  : DataFrame = {

    val resultOriginAiports =  flights.join(airports, flights("Origin") === airports("iata"), "inner")
    val resultDestAiports =  flights.join(airports, flights("Dest") === airports("iata"), "inner")
    val unionResult = resultOriginAiports
      .unionAll(resultDestAiports)
    unionResult
  }

  def getUSAFlights(dataFrame: DataFrame) : DataFrame={
    val result = dataFrame
      .filter(
        dataFrame("Month")>= "6"
          && dataFrame("Month")<= "8"
          && dataFrame("country")==="USA")
      .groupBy("airport")
      .count()

    result
  }
  def getNYFlights(dataFrame: DataFrame) : DataFrame={
    val result = dataFrame
      .filter(dataFrame("state")==="NY" && dataFrame("Month")==="6")
      .groupBy("state").count()

    result
  }
}
