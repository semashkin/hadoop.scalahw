package task2

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Semashkin on 08.09.2016.
  */
object Task2 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setMaster("local[*]").setAppName("Simple Application")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val flights = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("hdfs:///demo/Spark/Task2/2007.csv").select("Year","Month","UniqueCarrier","FlightNum","Origin","Dest")
      //.toDF("Year","Month","UniqueCarrier","FlightNum","Origin","Dest")
      val carriers = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("hdfs:///demo/Spark/Task2/carriers.csv")
      //.toDF("iata","airport","city","state","country","lat","long")
    val airports = sqlContext.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load("hdfs:///demo/Spark/Task2/airports.csv")
      /*.toDF("Code","Description")*/
    if(args(0)=="1" || args(0)=="4") {
      //Count total number of flights per carrier in 2007 (make #2 screenshot)
      val resultWithCarriers = CarriersService.get(flights,carriers)
        .groupBy("Description")
        .count()

      if (args(0)=="1") {
        val finalResult = resultWithCarriers.collect()
        println("1. Count total number of flights per carrier in 2007")
        finalResult.foreach(println)
      }
      if (args(0)=="4") {
        val finalResult = resultWithCarriers
          .orderBy(resultWithCarriers("count").desc)
          .collect()
        println("4. Find the carrier who served the biggest number of flights")
        finalResult.foreach(println)
      }

    }

    if(args(0)=="2" || args(0)=="3") {
      val unionResult = AirportService.getFlightPerAiport(flights, airports)
      //The total number of flights served in Jun 2007 by NYC (all airports, use join with Airports
      //data and don’t
      if(args(0)=="2") {
        val finalResult = AirportService.getNYFlights(unionResult)
          .collect()
        println("2. The total number of flights served in Jun 2007 by NYC")
        finalResult.foreach(println)
      }
      //Find five most busy airports in US during Jun 01 - Aug 31
      if(args(0)=="3") {
        val result2 =AirportService.getUSAFlights(unionResult)
        val finalResult = result2
          .orderBy(result2("count").desc)
          .collect()

        println("3. Find five most busy airports in US during Jun 01 - Aug 31")
        finalResult
          .take(5)
          .foreach(println)
      }
    }



    /*case class Data(
                     Year: Integer,
                     Month: Integer,
                     //DayofMonth: int,
                     //DayOfWeek: int,
                     //DepTime: int,
                     //CRSDepTime: string,
                     //ArrTime: string,
                     //CRSArrTime: string,
                     UniqueCarrier: String,
                     FlightNum: String,
                     //TailNum: string,
                     //ActualElapsedTime: int,
                     //CRSElapsedTime: int,
                     //AirTime: int,
                     //ArrDelay: int,
                     //DepDelay: int,
                     Origin: String,
                     Dest: String
                     //Distance: int,
                     //TaxiIn: int,
                     //TaxiOut: int,
                     //Cancelled: int,
                     //CancellationCode: int,
                     //Diverted: int,
                     //CarrierDelay: int,
                     //WeatherDelay: int,
                     //NASDelay: int,
                     //SecurityDelay: int,
                     //LateAircraftDelay: int
                   );

    case class airports(
                         iata: String,
                         airport: String,
                         city: String,
                         state: String,
                         country: String
                       )*/
    /*val parsedData = logData.map(_.split(",")).map(p => Data(p(0).toInt,p(1).toInt,p(8),p(9),p(16),p(17))).toDF();*/

  }
}
