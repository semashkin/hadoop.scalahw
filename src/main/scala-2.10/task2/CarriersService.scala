package task2

import org.apache.spark.sql.DataFrame

/**
  * Created by Semashkin on 11.09.2016.
  */
object CarriersService {
  def get(flights: DataFrame, carriers: DataFrame) : DataFrame ={
    val result = flights.join(carriers, flights("UniqueCarrier") === carriers("Code"), "inner")
    result
  }
}
