/**
  * Created by Semashkin on 11.09.2016.
  */
case class FlightData(Month:Integer, FlightNum:String, Origin:String, Dest:String,UniqueCarrier:String)

object Flight {
  def from(line:String):FlightData = {
    val f = line.split(",")
    FlightData(f(0).toInt,f(1),f(2),f(3),f(4))
  }
}