
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import task1.BytesCalculation
import testing.spark.SparkSpec

class BytesCalculationSpec extends FlatSpec with SparkSpec with GivenWhenThen with Matchers {
  private val lines = Array(
    "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"",
    "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\""
  )

  "lines" should "be grouped and avg and sum calculated" in {
    val rdd = sc.parallelize(lines,2)
    val result = BytesCalculation.Run(rdd)
    assert(result.count()===1)
    assert(result.first().ip==="ip1")
    assert(result.first().avg===40028)
    assert(result.first().total===40028*2)
  }
}