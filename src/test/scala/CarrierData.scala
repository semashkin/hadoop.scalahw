/**
  * Created by Semashkin on 11.09.2016.
  */
case class CarrierData(Code:String, Description:String)

object Carrier {
  def from(line:String):CarrierData = {
    val f = line.split(",")
    CarrierData(f(0),f(1))
  }
}