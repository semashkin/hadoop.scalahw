/**
  * Created by Semashkin on 05.09.2016.
  */
package testing.spark

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FlatSpec}

trait SparkSpec extends FlatSpec with BeforeAndAfter {

  private val master = "local[2]"
  private val appName = "example-spark"

  protected var sc: SparkContext = _
  protected var sqlCtx: SQLContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    sc = new SparkContext(conf)
    sqlCtx = new SQLContext(sc)
  }

  after {
    if (sc != null) {
      sc.stop()
    }
  }
}
