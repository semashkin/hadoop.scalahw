import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import task2.AirportService
import testing.spark.SparkSpec

/**
  * Created by Semashkin on 11.09.2016.
  */

class AirportServiceSpec extends FlatSpec with SparkSpec with GivenWhenThen with Matchers {
  private val flights = Seq(
    "6,123,1,2,4"
  )
  private val airpots = Seq(
    "1,USA,Burger,NY",
    "2,Russia,Matreshka,Les",
    "3,China,ChinaAir,NoWhere",
    "4,USA,Burger,C"
  )

  "getFlightPerAiport" should "return flight for origin and dest airport" in {
    val flightsDf = sqlCtx.createDataFrame(flights.map(Flight.from))
    val airportDf = sqlCtx.createDataFrame(airpots.map(Airport.from))
    val result = AirportService.getFlightPerAiport(flightsDf,airportDf)

    assert(2 == result.collect().length)
  }

  "getNYFlights" should "return should return count of flights in NY" in {
    val manyFlights = Seq(
      "6,1,1,2,4",
      "6,2,2,3,4",
      "6,3,3,4,4"
    )
    val flightsDf = sqlCtx.createDataFrame(manyFlights.map(Flight.from))
    val airportDf = sqlCtx.createDataFrame(airpots.map(Airport.from))
    val result = AirportService.getNYFlights(AirportService.getFlightPerAiport(flightsDf,airportDf))

    assert(1 == result.collect().length)
  }

  "getUSAFlights" should "return should return count of flights in USA" in {
    val manyFlights = Seq(
      "6,1,1,2,4",
      "7,2,2,3,4",
      "8,3,3,4,4",
      "9,4,3,4,4"
    )
    val flightsDf = sqlCtx.createDataFrame(manyFlights.map(Flight.from))
    val airportDf = sqlCtx.createDataFrame(airpots.map(Airport.from))
    val result = AirportService.getUSAFlights(AirportService.getFlightPerAiport(flightsDf,airportDf))
    result.collect().foreach(println)
    assert(1 == result.collect().length)
  }
}
