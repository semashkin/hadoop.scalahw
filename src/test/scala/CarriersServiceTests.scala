import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import task2.CarriersService
import testing.spark.SparkSpec

/**
  * Created by Semashkin on 11.09.2016.
  */
class CarriersServiceTests extends FlatSpec with SparkSpec with GivenWhenThen with Matchers {
  private val flights = Seq(
    "6,123,1,2,4",
    "6,123,1,2,3"
  )
  private val carriers = Seq(
    "3,SeomDescr",
    "2,SeomDescr1"
  )

  "get" should "return join with carriers" in {
    val flightsDf = sqlCtx.createDataFrame(flights.map(Flight.from))
    val carriersDf = sqlCtx.createDataFrame(carriers.map(Carrier.from))
    val result = CarriersService.get(flightsDf, carriersDf)

    assert(1 == result.collect().length)
  }
}