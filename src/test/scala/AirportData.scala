/**
  * Created by Semashkin on 11.09.2016.
  */
case class AirportData(iata:String, country:String, airport:String, state:String)

object Airport {
  def from(line:String):AirportData = {
    val f = line.split(",")
    AirportData(f(0),f(1),f(2),f(3))
  }
}