name := "Hadoop.ScalaHW"

version := "1.0"

scalaVersion := "2.10.5"
/*lazy val root = (project in file(".")).
  settings(
    name := "Hadoop.ScalaHW",
    version := "1.0",
    scalaVersion := "2.11.8"
  )*/

//assemblyJarName in assembly := "apps.jar"

//test in assembly := {}

parallelExecution in Test := false
libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.10" % "1.6.1" % Provided,
  "org.apache.spark" %% "spark-sql" % "1.6.1" % Provided,
  /*"org.apache.spark" %% "spark-streaming" % "1.6.1" % Provided,
  "org.apache.spark" %% "spark-streaming-kafka" % "1.6.1" % Provided,*/
  "org.apache.spark"  % "spark-mllib_2.10"             % "1.1.0",

  "com.databricks" %% "spark-csv" % "1.3.0",
  /*"org.elasticsearch" %% "elasticsearch-spark" % "2.3.2",
  "com.datastax.spark" %% "spark-cassandra-connector" % "1.6.0-M2",
  "com.restfb" % "restfb" % "1.23.0",
  "com.typesafe" % "config" % "1.3.0",
  "joda-time" % "joda-time" % "2.9.3",
  "eu.bitwalker" % "UserAgentUtils" % "1.19",*/
  "org.scalatest" % "scalatest_2.10" % "3.0.0"
  /* "com.holdenkarau" %% "spark-testing-base" % "1.6.1_0.3.3" % Test,
   "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % Test*/
)
resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

/*addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.7.0-SNAPSHOT")*/
run in Compile <<= Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))
runMain in Compile <<= Defaults.runMainTask(fullClasspath in Compile, runner in (Compile, run))
